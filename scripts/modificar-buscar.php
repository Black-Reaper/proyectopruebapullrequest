<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../materialize/materialize.css">
    <title>Web Juan - Alex, Sara E Ivan</title>
</head>

<body style="background-color: rgb(211,235,243);">
    <header></header>

    <!--Nav principal con resize-->
    <nav class="nav-extended" style="background-color: rgb(255, 133, 0);">
        <div class="nav-wrapper">
            <a href="../index.html" class="brand-logo">Web Alex, Sara E Iván</a>
            <a href="#" data-target="slide-out" class="sidenav-trigger">
                <i class="material-icons">Menú</i>
            </a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="../formularios/alta.html">Altas</a></li>
        <li><a href="../formularios/modificacion.html">Modificaciones</a></li>
        <li><a href="../formularios/consulta.html">Consultas</a></li>
        <li><a href="../formularios/baja.html">Bajas</a></li>
            </ul>
        </div>
    </nav>

    <!--Nav secundario mobile-->
    <ul class="sidenav" id="slide-out" style="transform: translateX(-105%);">
    <li><a href="../formularios/alta.html">Altas</a></li>
        <li><a href="../formularios/modificacion.html">Modificaciones</a></li>
        <li><a href="../formularios/consulta.html">Consultas</a></li>
        <li><a href="../formularios/baja.html">Bajas</a></li>
    </ul>

    <!--Main-->
    <main>
        <div class="container">
            <h4>Modificaciones</h4>
            <?php

require_once '../clases/Conexion.php';

try {
    //echo 'Conexion Correcta';
    $db = new PDO('mysql:host=' . $servidor . ';dbname=' . $bd, $usuario, $contraseña);
    if (isset($_POST['buscar'])) {

        $nombre = $_POST['nombre'];

        $sql1 = "SELECT dni,nombre,apellidos,email,fecha FROM clientes WHERE nombre =?";
        $consulta1 = $db->prepare($sql1);
        $consulta1->execute(array($nombre));
        $registro = $consulta1->fetch();

        if (!$registro) {
            print "El cliente no ha podido ser encontrado";
        } else {

            //existe al menos 1 registro
            //print($registro[0]. "    ".$registro['precio']);    
            echo '<form action="modificacion.php" method="post">
<table align="center" class="content-layout">
        <tr>
          <td align="right"><strong>DNI:</strong></td>
          <td>
            <input type="text" name="dni" readonly size="20" value="'
                . $registro[0] . '" />
          </td>
         </tr>
         <tr>
          <td align="right"><strong>Nombre :</strong></td>
          <td>
            <input type="text" name="nombre" size="55" value="'
                . $registro[1] . '" />
          </td>
         </tr>
         <tr>
          <td align="right"><strong>Apellidos :</strong></td>
          <td>
            <input type="text" name="apellidos" size="20" value="'
                . $registro[2] . '"/>
          </td>
         </tr>
         <tr>
         <td align="right"><strong>Email :</strong></td>
         <td>
           <input type="email" name="email" size="20" value="'
                . $registro[3] . '"/>
         </td>
        </tr>
        <tr>
        <td align="right"><strong>Fecha:</strong></td>
        <td>
          <input type="date" name="fecha" size="20" value="'
                . $registro[4] . '"/>
        </td>
       </tr>
         <tr>
            <td colspan="2">
            <div class="row">

                <input name="modificar" type="submit" value="Modificar" class="btn-large blue"/>
             </div>
            </td>
        </tr>
        </table>
        </form>';
        } //else

    }
} catch (PDOException $e) {

    echo 'Error conectando con la base de datos: ' . $e->getMessage();
}
?>
        </div>
    </main>

    <!--Footer-->
    <footer class="page-footer" style="background-color: rgb(0,191,171);">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="black-text">Créditos</h5>
                    <p class="grey-text text-lighten-4">Realizado con un modelo kanban board en trello, 3 repositorios privados para cada dev y 1 master.</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Materialize JS -->
    <script src="../materialize/materialize.min.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.sidenav');
            var instances = M.Sidenav.init(elems);
        });
    </script>
</body>

</html>