<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../materialize/materialize.css">
    <title>Web Juan - Alex, Sara E Ivan</title>
</head>

<body style="background-color: rgb(211,235,243);">
    <header></header>

    <!--Nav principal con resize-->
    <nav class="nav-extended" style="background-color: rgb(255, 133, 0);">
        <div class="nav-wrapper">
            <a href="../index.html" class="brand-logo">Web Alex, Sara E Iván</a>
            <a href="#" data-target="slide-out" class="sidenav-trigger">
                <i class="material-icons">Menú</i>
            </a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="../formularios/alta.html">Altas</a></li>
        <li><a href="../formularios/modificacion.html">Modificaciones</a></li>
        <li><a href="../formularios/consulta.html">Consultas</a></li>
        <li><a href="../formularios/baja.html">Bajas</a></li>
            </ul>
        </div>
    </nav>

    <!--Nav secundario mobile-->
    <ul class="sidenav" id="slide-out" style="transform: translateX(-105%);">
    <li><a href="../formularios/alta.html">Altas</a></li>
        <li><a href="../formularios/modificacion.html">Modificaciones</a></li>
        <li><a href="../formularios/consulta.html">Consultas</a></li>
        <li><a href="../formularios/baja.html">Bajas</a></li>
    </ul>

    <!--Main-->
    <main>
        <div class="container">
            <h4>Altas</h4>
            <?php

require_once "../clases/conexion.php";
        try {

            $db = new PDO('mysql:host='.$servidor.
                ';dbname='.$bd, $usuario, $contraseña);
            
            if (isset($_POST['insertar'])) {

                //insercion
                $DNI = $_POST['DNI'];
                $nombre = $_POST['nombre'];
                $apellido = $_POST['apellido'];
                //$contraseña=md5($_POST['contraseña']);
                $password = $_POST['contraseña'];
                //y ahora cifro la clave usando un hash
                // $clave_cifrada = password_hash($contraseña, PASSWORD_DEFAULT, array("cost" => 15));
                //$claveCifrada = password_hash($password, PASSWORD_BCRYPT, array("cost" => 15));
                /*
                el "cost" lo que indica es la fuerza con la que se cifra la clave
                a mayor fuerza mas tiempo de carga requiere la página por eso es
                importante que busques un equilibrio
                */
                //$hash=password_hash($contraseña, \PASSWORD_DEFAULT);
                $fecha = $_POST['fecha'];
                $email = $_POST['email'];

                //$sql="insert into articulos (descripcion,precio) values (:des,:pre)";
                $sql = "insert into Clientes (dni,nombre,apellidos,psw,email,fecha) values (?,?,?,?,?,?)";
                $consulta = $db -> prepare($sql);


                //$result = $consulta -> execute(array(":des" => $des, ":pre" => $pre));  
                $result = $consulta -> execute(array($DNI, $nombre, $apellido, $password, $email, $fecha));


                echo "<h4>Inserción correcta</h4>";
            }

        } catch (PDOException $a) {

            print('Error conectando con la base de datos: '.$e -> getMessage());
        }
?>

        </div>
    </main>

    <!--Footer-->
    <footer class="page-footer" style="background-color: rgb(0,191,171);">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="black-text">Créditos</h5>
                    <p class="grey-text text-lighten-4">Realizado con un modelo kanban board en trello, 3 repositorios privados para cada dev y 1 master.</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Materialize JS -->
    <script src="../materialize/materialize.min.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.sidenav');
            var instances = M.Sidenav.init(elems);
        });
    </script>
</body>

</html>